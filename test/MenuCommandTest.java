

import library.LibraryController;
import menu.MenuController;
import menucommands.*;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.verify;

public class MenuCommandTest {

    @Test
    public void list_of_available_books_is_shown_on_users_choice() {

        LibraryController libraryController = Mockito.mock(LibraryController.class);
        MenuCommand showBooksCommand = new ShowBooksCommand(libraryController);

        showBooksCommand.execute();
        verify(libraryController).showItems();
    }


    @Test
    public void application_quits_on_users_choice() {

        LibraryController libraryController = Mockito.mock(LibraryController.class);
        MenuCommand exitApplicationCommand = new ExitApplicationCommand(libraryController);

        exitApplicationCommand.execute();
        verify(libraryController).quit();
    }

    @Test
    public void error_message_displayed_when_invalid_option_is_entered() {

        MenuController menuController = Mockito.mock(MenuController.class);
        MenuCommand displayErrorCommand = new DisplayErrorCommand(menuController);

        displayErrorCommand.execute();
        verify(menuController).showInvalidInputMessage();
    }


    @Test
    public void application_asks_for_a_bookname_when_user_chooses_to_issue_a_book() {


        LibraryController libraryController = Mockito.mock(LibraryController.class);
        MenuCommand checkoutBook = new CheckoutBookCommand(libraryController);

        checkoutBook.execute();
        verify(libraryController).checkoutItem();
    }

    @Test
    public void application_asks_for_book_id_when_user_wants_to_return_a_book() {

        LibraryController libraryController = Mockito.mock(LibraryController.class);
        MenuCommand returnBookCommand = new ReturnBookCommand(libraryController);

        returnBookCommand.execute();
        verify(libraryController).performItemReturn();
    }

    @Test
    public void list_of_available_movies_is_shown_on_users_choice() {

        LibraryController libraryController = Mockito.mock(LibraryController.class);
        MenuCommand showMoviesCommand = new ShowMoviesCommand(libraryController);

        showMoviesCommand.execute();
        verify(libraryController).showItems();
    }

}
