import Items.*;
import authentication.InvalidLoggedInUserException;
import customer.Customer;
import customer.Customers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ItemsTest {

    @Test
    public void book_is_successfully_returned_to_the_library_if_it_was_already_checkedout_and_user_is_logged_in() throws ItemNotFoundException, InvalidLoggedInUserException {

        Customers customers = Mockito.mock(Customers.class);
        Books books = new Books(customers);
        books.loadAllItemsIntoLibrary();
        Customer customer = mock(Customer.class);

        when(customers.getLoggedInCustomer()).thenReturn(customer);

        Item issuedBook = books.issueItem("To Kill a MockingBird");
        int issedBookId = issuedBook.getItemId();
        assertTrue(books.returnItem(issedBookId));

    }

    @Test
    public void book_is_not_returned_to_the_library_if_it_is_not_checkedout_from_the_library() throws ItemNotFoundException, InvalidLoggedInUserException {

        Customers customers = Mockito.mock(Customers.class);
        Books books = new Books(customers);
        books.loadAllItemsIntoLibrary();


        Book book = new Book(1, "To Kill a MockingBird", "Harper Lee", 1963);
        int bookId = book.getItemId();
        assertFalse(books.returnItem(bookId));

    }

    @Test
    public void movie_is_checked_out_successfully_if_it_exists_and_user_is_logged_in() throws ItemNotFoundException, InvalidLoggedInUserException {

        Customers customers = Mockito.mock(Customers.class);
        Items movies = new Movies(customers);
        movies.loadAllItemsIntoLibrary();
        Customer customer = mock(Customer.class);

        when(customers.getLoggedInCustomer()).thenReturn(customer);

        Item issuedMovie = movies.issueItem("The Shawshank Redemption");
        assertEquals("The Shawshank Redemption", issuedMovie.getName());

    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void movie_is_not_checked_out_if_it_does_not_exist() throws ItemNotFoundException, InvalidLoggedInUserException {
        expectedException.expect(ItemNotFoundException.class);

        Customers customers = Mockito.mock(Customers.class);
        Customer customer = mock(Customer.class);

        when(customers.getLoggedInCustomer()).thenReturn(customer);

        Movies movies = new Movies(customers);
        movies.loadAllItemsIntoLibrary();

        movies.issueItem("Hitman");
    }


    @Test
    public void item_is_not_checked_out_if_user_is_not_logged_in() throws InvalidLoggedInUserException, ItemNotFoundException {
        expectedException.expect(InvalidLoggedInUserException.class);
        Customers customers = Mockito.mock(Customers.class);

        when(customers.getLoggedInCustomer()).thenReturn(null);

        Movies movies = new Movies(customers);
        movies.loadAllItemsIntoLibrary();

        movies.issueItem("Hitman");
    }


    @Test
    public void checkedout_item_cannot_be_returned_when_user_no_user_logged_in() throws InvalidLoggedInUserException, ItemNotFoundException {

        expectedException.expect(InvalidLoggedInUserException.class);

        Customers customers = new Customers();
        customers.loadAllCustomers();
        customers.verifyCustomer("123-1234", "password");

        Books books = new Books(customers);
        Book book = new Book(1, "To Kill a MockingBird", "Harper Lee", 1963);

        books.loadAllItemsIntoLibrary();

        books.issueItem("To Kill a MockingBird");
        customers.logoutCurrentlyLoggedInCustomer();

        int bookId = book.getItemId();
        books.returnItem(bookId);
    }
}
