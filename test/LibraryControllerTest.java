import Items.*;
import authentication.InvalidLoggedInUserException;
import library.LibraryController;
import library.LibraryView;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class LibraryControllerTest {


    @Test
    public void application_successfully_reads_the_details_of_all_books_and_displays_them() {

        LibraryView libraryView = Mockito.mock(LibraryView.class);
        Books books = Mockito.mock(Books.class);

        List<String> detailsOfAllBooks = Arrays.asList("To Kill a MockingBird\t|Harper Lee\t|1963",
                "A Thousand Splendid Sons\t|Khaled Hosseini\t|2003",
                "Kite Runner\t|Khaled Hosseini\t|2008");

        when(books.detailsOfAllItems()).thenReturn(detailsOfAllBooks);

        LibraryController libraryController = new LibraryController(books, libraryView);
        libraryController.showItems();

        verify(libraryView).showItems(detailsOfAllBooks);

    }


    @Test
    public void application_is_able_to_checkout_a_book_successfully_if_it_exists() throws ItemNotFoundException, InvalidLoggedInUserException {

        LibraryView libraryView = Mockito.mock(LibraryView.class);
        Books books = Mockito.mock(Books.class);

        Book book = new Book(1, "A book", "Some guy", 2072);

        when(libraryView.getNameOfItemToBeCheckedout()).thenReturn("A book");
        when(books.issueItem("A book")).thenReturn(book);

        LibraryController libraryController = new LibraryController(books, libraryView);
        libraryController.checkoutItem();

        verify(libraryView).showItemCheckedoutMessage(book.details());

    }


    @Test
    public void application_is_not_able_to_checkout_a_book_if_it_does_not_exist() throws ItemNotFoundException, InvalidLoggedInUserException {


        LibraryView libraryView = Mockito.mock(LibraryView.class);
        Books books = Mockito.mock(Books.class);

        Book book = new Book(1, "A book", "Some guy", 2072);

        when(libraryView.getNameOfItemToBeCheckedout()).thenReturn("A book");
        when(books.issueItem("A book")).thenThrow(new ItemNotFoundException());

        LibraryController libraryController = new LibraryController(books, libraryView);
        libraryController.checkoutItem();

        verify(libraryView).displayItemNotFoundMessage();
        verify(libraryView, never()).showItemCheckedoutMessage(book.details());

    }


    @Test
    public void user_successfully_returns_an_issued_book() throws ItemNotFoundException, InvalidLoggedInUserException {

        LibraryView libraryView = Mockito.mock(LibraryView.class);
        Books books = Mockito.mock(Books.class);

        when(libraryView.getIdOfItemToBeReturned()).thenReturn(1);
        when(books.returnItem(1)).thenReturn(true);

        LibraryController libraryController = new LibraryController(books, libraryView);

        libraryController.performItemReturn();


        verify(libraryView).showItemSuccessfullyReturnedMessage();
        verify(libraryView, never()).displayInvalidItemError();

    }

    @Test
    public void user_cannot_return_an_unissued_book() throws ItemNotFoundException, InvalidLoggedInUserException {

        LibraryView libraryView = Mockito.mock(LibraryView.class);
        Books books = Mockito.mock(Books.class);

        when(libraryView.getIdOfItemToBeReturned()).thenReturn(1);
        when(books.returnItem(1)).thenReturn(false);

        LibraryController libraryController = new LibraryController(books, libraryView);

        libraryController.performItemReturn();

        verify(libraryView, never()).showItemSuccessfullyReturnedMessage();
        verify(libraryView).displayInvalidItemError();

    }


    @Test
    public void application_reads_the_details_of_all_available_movies_and_displays_them() {

        LibraryView libraryView = Mockito.mock(LibraryView.class);
        Books books = Mockito.mock(Books.class);

        List<String> detailsOfAllMovies =
                Arrays.asList("Three Colors White\t|Kryzstof Kieslowski\t|1994\t|8",
                        "The Shawshank Redemption\t|Frank Darabont\t|1994\t|10",
                        "Life is Beautiful\tRoberto Benigni\t|1997\t|9");

        when(books.detailsOfAllItems()).thenReturn(detailsOfAllMovies);

        LibraryController libraryController = new LibraryController(books, libraryView);
        libraryController.showItems();

        verify(libraryView).showItems(detailsOfAllMovies);
    }


    @Test
    public void application_is_able_to_checkout_a_movies_successfully_if_it_exists() throws ItemNotFoundException, InvalidLoggedInUserException {

        LibraryView libraryView = Mockito.mock(LibraryView.class);
        Movies movies = Mockito.mock(Movies.class);

        Movie movie = new Movie(2, "Inception", "Christopher Nolan", 2010, 8);

        when(libraryView.getNameOfItemToBeCheckedout()).thenReturn("Inception");
        when(movies.issueItem("Inception")).thenReturn(movie);

        LibraryController libraryController = new LibraryController(movies, libraryView);
        libraryController.checkoutItem();

        verify(libraryView).showItemCheckedoutMessage(movie.details());
    }


    @Test
    public void application_is_not_able_to_checkout_a_movie_if_it_does_not_exist() throws ItemNotFoundException, InvalidLoggedInUserException {


        LibraryView libraryView = Mockito.mock(LibraryView.class);
        Items movies = Mockito.mock(Books.class);

        Movie movie = new Movie(2, "Inception", "Christopher Nolan", 2010, 8);

        when(libraryView.getNameOfItemToBeCheckedout()).thenReturn("Inception");
        when(movies.issueItem("Inception")).thenThrow(new ItemNotFoundException());

        LibraryController libraryController = new LibraryController(movies, libraryView);
        libraryController.checkoutItem();

        verify(libraryView).displayItemNotFoundMessage();
        verify(libraryView, never()).showItemCheckedoutMessage(movie.details());

    }


}

