import customer.Customer;
import customer.Customers;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomersTest {

    @Test
    public void credentials_of_existing_customer_successfully_verified() {

        Customers customers = new Customers();
        customers.loadAllCustomers();
        boolean verified = customers.verifyCustomer("123-1234", "password");
        assertTrue(verified);
    }


    @Test
    public void credentials_of_non_existent_customer_cannot_be_verified() {

        Customers customers = new Customers();
        customers.loadAllCustomers();
        boolean verified = customers.verifyCustomer("123-12345", "password");
        assertFalse(verified);
    }


    @Test
    public void credentials_with_wrong_password_cannot_be_verified() {

        Customers customers = new Customers();
        customers.loadAllCustomers();

        boolean verified = customers.verifyCustomer("123-1234", "abc");
        assertFalse(verified);
    }


    @Test
    public void customer_logged_in_successfully_when_credentials_are_correct() {

        Customers customers = new Customers();
        customers.loadAllCustomers();
        Customer customer = new Customer("Souvik", "123-1234", "9049766840", "souvik014@gmail.com", "password");

        customers.verifyCustomer("123-1234", "password");
        Customer expectedCustomer = customers.getLoggedInCustomer();

        assertEquals(customer, expectedCustomer);

    }


    @Test
    public void customer_logged_out_successfully_after_login() {

        Customers customers = new Customers();
        customers.loadAllCustomers();

        customers.verifyCustomer("123-1234", "password");
        customers.logoutCurrentlyLoggedInCustomer();

        assertEquals(null, customers.getLoggedInCustomer());
    }


    @Test
    public void user_details_are_shown_when_user_is_logged_in() {

        Customers customers = new Customers();
        customers.loadAllCustomers();
        customers.verifyCustomer("123-1234", "password");

        String details = customers.getCurrentUserDetails();
        assertEquals("Souvik 9049766840 souvik014@gmail.com", details);
    }

}
