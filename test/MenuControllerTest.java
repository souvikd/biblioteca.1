import io.InputReader;
import io.OutputWriter;
import menu.Menu;
import menu.MenuController;
import menu.MenuView;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MenuControllerTest {

    @Test
    public void application_successfully_reads_menu_items_and_displays_them() {

        Menu menu = Mockito.mock(Menu.class);
        MenuView menuView = Mockito.mock(MenuView.class);
        List<String> menuItems = Collections.singletonList("1.Show all books.");

        when(menu.allMenuItems()).thenReturn(menuItems);

        MenuController menuController = new MenuController(menu, menuView);
        menuController.showMenu();

        verify(menuView).showMenu(menuItems);
    }

    @Test
    public void application_gets_1_as_input_when_user_enters_1() {
        Menu menu = Mockito.mock(Menu.class);
        InputReader reader = Mockito.mock(InputReader.class);
        OutputWriter writer = Mockito.mock(OutputWriter.class);
        MenuView menuView = new MenuView(reader, writer);

        when(reader.readMenuChoice()).thenReturn(1);

        MenuController menuController = new MenuController(menu, menuView);
        int choice = menuController.getUserChoice();
        assertEquals(1, choice);

    }


    @Test
    public void application_successfully_displays_error_message(){
        Menu menu = Mockito.mock(Menu.class);
        InputReader reader = Mockito.mock(InputReader.class);
        OutputWriter writer = Mockito.mock(OutputWriter.class);
        MenuView menuView = new MenuView(reader, writer);



        MenuController menuController = new MenuController(menu, menuView);
        menuController.showInvalidInputMessage();

        verify(writer).write("Select a valid option!");


    }

}
