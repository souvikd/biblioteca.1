import authentication.AuthenticationController;
import customer.Customers;
import mainapp.AppView;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AuthenticationControllerTest {

    @Test
    public void successful_log_in_message_displayed_on_successful_login() {

        Customers customers = Mockito.mock(Customers.class);
        AppView appView = Mockito.mock(AppView.class);

        AuthenticationController authenticationController = new AuthenticationController(customers, appView);

        when(appView.getUserCredentials()).thenReturn(Arrays.asList("abc", "def"));
        when(customers.verifyCustomer("abc", "def")).thenReturn(true);
        authenticationController.login();

        verify(appView).displayLoginSuccessfulMessage();
    }

    @Test
    public void fail_log_in_attempt_message_displayed_on_unsuccessful_login() {

        Customers customers = Mockito.mock(Customers.class);
        AppView appView = Mockito.mock(AppView.class);

        AuthenticationController authenticationController = new AuthenticationController(customers, appView);

        when(appView.getUserCredentials()).thenReturn(Arrays.asList("abc", "def"));
        when(customers.verifyCustomer("abc", "def")).thenReturn(false);
        authenticationController.login();

        verify(appView).displaySignInError();
    }

}