package menucommands;

public interface MenuCommand {
    void execute();
}
