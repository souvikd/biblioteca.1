package menucommands;

import library.LibraryController;

public class ShowMoviesCommand implements MenuCommand {
    private LibraryController moviesController;

    public ShowMoviesCommand(LibraryController moviesController) {

        this.moviesController = moviesController;
    }

    @Override
    public void execute() {
        moviesController.showItems();
    }
}
