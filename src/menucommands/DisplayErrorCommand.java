package menucommands;


import library.LibraryController;
import menu.MenuController;

public class DisplayErrorCommand implements MenuCommand {


    private MenuController menuController;

    public DisplayErrorCommand(MenuController menuController) {
        this.menuController = menuController;
    }

    @Override
    public void execute() {
        menuController.showInvalidInputMessage();
    }
}
