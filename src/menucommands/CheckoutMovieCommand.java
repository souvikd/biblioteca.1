package menucommands;

import library.LibraryController;

public class CheckoutMovieCommand implements MenuCommand {
    private LibraryController moviesController;

    public CheckoutMovieCommand(LibraryController moviesController) {

        this.moviesController = moviesController;
    }

    @Override
    public void execute() {
        moviesController.checkoutItem();
    }
}
