package menucommands;

import library.LibraryController;

public class ExitApplicationCommand implements MenuCommand {
    private LibraryController libraryController;

    public ExitApplicationCommand(LibraryController libraryController) {
        this.libraryController = libraryController;
    }

    @Override
    public void execute() {
        libraryController.quit();
    }
}
