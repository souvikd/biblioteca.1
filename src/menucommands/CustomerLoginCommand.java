package menucommands;

import authentication.AuthenticationController;

public class CustomerLoginCommand implements MenuCommand {
    private AuthenticationController authenticationController;

    public CustomerLoginCommand(AuthenticationController authenticationController) {

        this.authenticationController = authenticationController;
    }

    @Override
    public void execute() {
        authenticationController.login();
    }
}
