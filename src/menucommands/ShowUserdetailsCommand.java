package menucommands;

import authentication.AuthenticationController;
import library.LibraryController;
import mainapp.AppView;

public class ShowUserdetailsCommand implements MenuCommand {

    private AuthenticationController authenticationController;

    public ShowUserdetailsCommand(AuthenticationController authenticationController) {
        this.authenticationController = authenticationController;
    }

    @Override
    public void execute() {
        authenticationController.showCurrentlyLoggedInCustomerDetails();
    }
}
