package menucommands;

import library.LibraryController;

public class ShowBooksCommand implements MenuCommand {

    private LibraryController libraryController;

    public ShowBooksCommand(LibraryController libraryController) {
        this.libraryController = libraryController;
    }

    public void execute(){
        libraryController.showItems();
    }
}
