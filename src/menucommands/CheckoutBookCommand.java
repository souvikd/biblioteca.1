package menucommands;

import library.LibraryController;

public class CheckoutBookCommand implements MenuCommand {
    private LibraryController libraryController;

    public CheckoutBookCommand(LibraryController libraryController) {

        this.libraryController = libraryController;
    }

    @Override
    public void execute() {
        libraryController.checkoutItem();
    }
}
