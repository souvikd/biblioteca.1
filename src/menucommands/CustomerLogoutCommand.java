package menucommands;

import authentication.AuthenticationController;

public class CustomerLogoutCommand implements MenuCommand {

    private AuthenticationController authenticationController;

    public CustomerLogoutCommand(AuthenticationController authenticationController) {
        this.authenticationController = authenticationController;
    }

    @Override
    public void execute() {
        authenticationController.logout();
    }

}
