package menucommands;

import library.LibraryController;

public class ReturnBookCommand implements MenuCommand{
    private LibraryController libraryController;

    public ReturnBookCommand(LibraryController libraryController) {
        this.libraryController = libraryController;
    }

    @Override
    public void execute() {
        libraryController.performItemReturn();
    }
}
