package io;

import java.io.PrintStream;

public class OutputWriter {

    private PrintStream stream;

    public OutputWriter(PrintStream stream) {
        this.stream = stream;
    }

    public void write(String message) {
        this.stream.println(message);
    }


}
