package io;

import java.io.InputStream;
import java.util.Scanner;

public class InputReader {

    private InputStream reader;

    public InputReader(InputStream reader) {
        this.reader = reader;
    }

    public int readMenuChoice() {
        Scanner scanner = new Scanner(reader);
        return scanner.nextInt();
    }

    public String readString() {
        Scanner scanner = new Scanner(reader);
        return scanner.nextLine();
    }

    public int getIdOfBookToBeReturned() {
        Scanner scanner = new Scanner(reader);
        return scanner.nextInt();
    }

    public String getNameOfMovieToBeCheckedout() {
        Scanner scanner = new Scanner(reader);
        return scanner.next();
    }
}
