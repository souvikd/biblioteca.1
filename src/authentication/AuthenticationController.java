package authentication;

import customer.Customers;
import mainapp.AppView;

import java.util.List;

public class AuthenticationController {

    private Customers customers;
    private AppView appView;

    public AuthenticationController(Customers customers, AppView appView) {
        this.customers = customers;
        this.appView = appView;
    }

    public void login() {

        List<String> userInformation = appView.getUserCredentials();
        boolean successfullyVerified = customers.verifyCustomer(userInformation.get(0), userInformation.get(1));

        if (successfullyVerified)
            appView.displayLoginSuccessfulMessage();
        else
            appView.displaySignInError();
    }

    public void logout() {
        customers.logoutCurrentlyLoggedInCustomer();
        appView.displayUserLoggedOutMessage();
    }

    public void showCurrentlyLoggedInCustomerDetails() {
        String customerDetails = customers.getCurrentUserDetails();
        if (customerDetails != null)
            appView.displayCurrentUserDetails(customerDetails);
        else
            appView.displayUserNotLoggedInMessage();
    }
}
