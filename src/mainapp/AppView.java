package mainapp;

import io.InputReader;
import io.OutputWriter;

import java.util.ArrayList;
import java.util.List;

public class AppView {

    private InputReader reader;
    private OutputWriter writer;

    public AppView(InputReader reader, OutputWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    public void welcomeUser() {
        writer.write("Hello and Welcome! Your app is ready.");
    }

    public List<String> getUserCredentials() {
        List<String> userCredentials = new ArrayList<>(2);

        writer.write("Enter user name.");
        userCredentials.add(0, reader.readString());

        writer.write("Enter password.");
        userCredentials.add(1, reader.readString());

        return userCredentials;
    }

    public void displaySignInError() {
        writer.write("Invalid username/password.");
    }

    public void displayLoginSuccessfulMessage() {
        writer.write("Logged in.");
    }

    public void displayUserLoggedOutMessage() {
        writer.write("Successfully logged out.");
    }


    public void displayUserNotLoggedInMessage() {
        writer.write("Please log in to continue.");
    }

    public void displayCurrentUserDetails(String customerDetails) {
        writer.write(customerDetails);
    }
}
