package mainapp;

import Items.Books;
import Items.Movies;
import customer.Customers;
import library.LibraryController;
import menu.MenuActions;
import menu.MenuController;
import menucommands.MenuCommand;

public class App {

    private final LibraryController moviesController;
    private final Customers customers;
    private LibraryController booksController;
    private MenuController menuController;
    private MenuActions menuActions;
    private AppView appView;
    private Dependencies dependencies;

    public App(Dependencies dependencies) {
        this.dependencies = dependencies;

        booksController = (LibraryController) dependencies.get(Books.class);
        menuController = (MenuController) dependencies.get(MenuController.class);
        moviesController = (LibraryController) dependencies.get(Movies.class);
        menuActions = (MenuActions) dependencies.get(MenuActions.class);
        appView = (AppView) dependencies.get(AppView.class);
        customers = (Customers) dependencies.get(Customers.class);

    }

    public void start() {


        appView.welcomeUser();
        booksController.loadItemsIntoLibrary();
        moviesController.loadItemsIntoLibrary();
        customers.loadAllCustomers();

        while (true) {
            menuController.showMenu();
            int choice = menuController.getUserChoice();
            Class<?> commandClass = menuActions.getCommandClass(choice);
            MenuCommand command = (MenuCommand) dependencies.get(commandClass);
            command.execute();
        }

    }
}
