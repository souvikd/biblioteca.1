package mainapp;

import Items.Books;
import Items.Items;
import Items.Movies;
import authentication.AuthenticationController;
import customer.Customers;
import io.InputReader;
import io.OutputWriter;
import library.LibraryController;
import library.LibraryView;
import menu.Menu;
import menu.MenuActions;
import menu.MenuController;
import menu.MenuView;
import menucommands.*;

import java.util.HashMap;

public class Dependencies {

    private HashMap<Class<?>, Object> objectHashMap;

    public Dependencies() {
        objectHashMap = new HashMap<>();
    }

    public void addToObjectSet(Class<?> claas, Object object) {
        objectHashMap.put(claas, object);
    }

    public Dependencies init() {

        OutputWriter writer = new OutputWriter(System.out);
        InputReader reader = new InputReader(System.in);

        Customers customers = new Customers();

        Items books = new Books(customers);
        Items movies = new Movies(customers);

        LibraryView libraryView = new LibraryView(reader, writer);
        Menu menu = new Menu(customers);
        MenuView menuView = new MenuView(reader, writer);
        LibraryController booksController = new LibraryController(books, libraryView);
        LibraryController moviesController = new LibraryController(movies, libraryView);

        AppView appView = new AppView(reader, writer);

        AuthenticationController authenticationController = new AuthenticationController(customers, appView);

        CustomerLoginCommand customerLoginCommand = new CustomerLoginCommand(authenticationController);
        CustomerLogoutCommand customerLogoutCommand = new CustomerLogoutCommand(authenticationController);
        ShowUserdetailsCommand showUserdetailsCommand = new ShowUserdetailsCommand(authenticationController);
        MenuActions menuActions = new MenuActions(customers);
        MenuController menuController = new MenuController(menu, menuView);
        ShowBooksCommand showBooksCommand = new ShowBooksCommand(booksController);
        ExitApplicationCommand exitApplicationCommand = new ExitApplicationCommand(booksController);
        DisplayErrorCommand displayErrorCommand = new DisplayErrorCommand(menuController);
        CheckoutBookCommand checkoutBookCommand = new CheckoutBookCommand(booksController);
        ReturnBookCommand returnBookCommand = new ReturnBookCommand(booksController);
        ShowMoviesCommand showMoviesCommand = new ShowMoviesCommand(moviesController);
        CheckoutMovieCommand checkoutMovieCommand = new CheckoutMovieCommand(moviesController);


        objectHashMap.put(OutputWriter.class, writer);
        objectHashMap.put(InputReader.class, reader);
        objectHashMap.put(Books.class, books);
        objectHashMap.put(LibraryView.class, libraryView);
        objectHashMap.put(Menu.class, menu);
        objectHashMap.put(MenuView.class, menuView);
        objectHashMap.put(Books.class, booksController);
        objectHashMap.put(Movies.class, moviesController);
        objectHashMap.put(MenuActions.class, menuActions);
        objectHashMap.put(MenuController.class, menuController);
        objectHashMap.put(AppView.class, appView);
        objectHashMap.put(ShowBooksCommand.class, showBooksCommand);
        objectHashMap.put(ExitApplicationCommand.class, exitApplicationCommand);
        objectHashMap.put(DisplayErrorCommand.class, displayErrorCommand);
        objectHashMap.put(CheckoutBookCommand.class, checkoutBookCommand);
        objectHashMap.put(ReturnBookCommand.class, returnBookCommand);
        objectHashMap.put(ShowMoviesCommand.class, showMoviesCommand);
        objectHashMap.put(CheckoutMovieCommand.class, checkoutMovieCommand);
        objectHashMap.put(AuthenticationController.class, authenticationController);
        objectHashMap.put(CustomerLoginCommand.class, customerLoginCommand);
        objectHashMap.put(Customers.class, customers);
        objectHashMap.put(CustomerLogoutCommand.class, customerLogoutCommand);
        objectHashMap.put(ShowUserdetailsCommand.class, showUserdetailsCommand);

        return this;
    }

    public Object get(Class<?> claass) {
        return objectHashMap.get(claass);
    }
}
