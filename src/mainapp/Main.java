package mainapp;

public class Main {
    public static void main(String[] args) {

        Dependencies dependencies = new Dependencies().init();

        App app = new App(dependencies);
        app.start();

    }
}
