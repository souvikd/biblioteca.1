package Items;

import authentication.InvalidLoggedInUserException;

import java.util.List;

public interface Items {

    List<Item> checkedOutItems = null;

    void loadAllItemsIntoLibrary();

    List<String> detailsOfAllItems();

    Item issueItem(String itemName) throws ItemNotFoundException, InvalidLoggedInUserException;

    boolean returnItem(int issuedBookId) throws InvalidLoggedInUserException;
}
