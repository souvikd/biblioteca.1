package Items;

import java.util.List;

public interface Item {
    String details();

    int getItemId();

    String getName();
}
