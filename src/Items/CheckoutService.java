package Items;

import authentication.InvalidLoggedInUserException;
import customer.Customer;
import customer.Customers;

import java.util.HashMap;
import java.util.List;

public class CheckoutService {


    Item checkoutItem(String itemName, List<Item> items, HashMap<Item, Customer> checkedOutItems, Customers customers) throws ItemNotFoundException, InvalidLoggedInUserException {

        if (customers.getLoggedInCustomer() == null)
            throw new InvalidLoggedInUserException();
        for (Item item : items) {
            if (item.details().indexOf(itemName) == 0) {
                checkedOutItems.put(item, customers.getLoggedInCustomer());
                items.remove(item);
                return item;
            }
        }
        throw new ItemNotFoundException();
    }
}
