package Items;


import authentication.InvalidLoggedInUserException;
import customer.Customer;
import customer.Customers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Books implements Items {

    private List<Item> books;
    private HashMap<Item, Customer> checkedOutItems;
    private Customers customers;


    public Books(Customers customers) {
        this.customers = customers;
    }

    @Override
    public void loadAllItemsIntoLibrary() {
        books = new ArrayList<>();
        books.add(new Book(1, "To Kill a MockingBird", "Harper Lee", 1963));
        books.add(new Book(2, "A Thousand Splendid Sons", "Khaled Hosseini", 2003));
        books.add(new Book(3, "Kite Runner", "Khaled Hosseini", 2008));
        checkedOutItems = new HashMap<>();
    }


    @Override
    public List<String> detailsOfAllItems() {
        return new InformationService().getItemDetails(books);
    }


    @Override
    public Item issueItem(String itemName) throws ItemNotFoundException, InvalidLoggedInUserException {
        return new CheckoutService().checkoutItem(itemName, books, checkedOutItems, customers);
    }


    @Override
    public boolean returnItem(int issuedBookId) throws InvalidLoggedInUserException {
        return new ReturnService().returnItem(issuedBookId, books, checkedOutItems, customers);
    }


}

