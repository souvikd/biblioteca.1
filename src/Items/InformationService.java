package Items;

import java.util.ArrayList;
import java.util.List;

public class InformationService {

    public List<String> getItemDetails(List<Item> items) {
        List<String> detailsOfAllItems = new ArrayList<>();
        for (Item item : items)
            detailsOfAllItems.add(item.details());
        return detailsOfAllItems;
    }
}
