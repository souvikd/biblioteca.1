package Items;

import authentication.InvalidLoggedInUserException;
import customer.Customer;
import customer.Customers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Movies implements Items {

    private List<Item> movies;
    private HashMap<Item, Customer> checkedOutMovies;
    private Customers customers;


    public Movies(Customers customers) {
        this.customers = customers;
    }

    public void loadAllItemsIntoLibrary() {
        movies = new ArrayList<>();
        movies.add(new Movie(2, "Inception", "Christopher Nolan", 2010, 8));
        movies.add(new Movie(1, "Three Colors White", "Kryzstof Kieslowski", 1994, 8));
        movies.add(new Movie(3, "The Shawshank Redemption", "Frank Darabont", 1994, 10));
        checkedOutMovies = new HashMap<>();
    }


    public List<String> detailsOfAllItems() {
        return new InformationService().getItemDetails(movies);
    }

    public Item issueItem(String movieName) throws ItemNotFoundException, InvalidLoggedInUserException {
        return new CheckoutService().checkoutItem(movieName, movies, checkedOutMovies, customers);
    }

    @Override
    public boolean returnItem(int issuedBookId) throws InvalidLoggedInUserException {
        return false;
    }


}
