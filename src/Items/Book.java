package Items;

public class Book implements Item {
    private final String title;
    private final String author;
    private final int year;
    private final int bookId;

    public Book(int bookId, String title, String author, int year) {
        this.title = title;
        this.author = author;
        this.year = year;
        this.bookId = bookId;
    }

    public String details() {
        return title + "\t|" + author + "\t|" + year;
    }


    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;

        Book book = (Book) other;

        return bookId == book.bookId;

    }

    @Override
    public int hashCode() {
        return bookId;
    }

    public int getItemId() {
        return bookId;
    }

    @Override
    public String getName() {
        return title;
    }
}
