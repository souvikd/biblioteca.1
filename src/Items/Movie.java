package Items;

public class Movie implements Item {

    private final String name;
    private final String director;
    private final int yearOfRelease;
    private final int rating;
    private int movieId;

    public Movie(int movieId, String name, String director, int yearOfRelease, int rating) {

        this.movieId = movieId;
        this.name = name;
        this.director = director;
        this.yearOfRelease = yearOfRelease;
        this.rating = rating;

    }

    public String details() {
        return name + "\t|" + director + "\t|" + yearOfRelease + "\t|" + rating;
    }

    @Override
    public int getItemId() {
        return movieId;
    }

    @Override
    public String getName() {
        return name;
    }
}
