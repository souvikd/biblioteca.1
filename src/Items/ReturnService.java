package Items;

import authentication.InvalidLoggedInUserException;
import customer.Customer;
import customer.Customers;

import java.util.HashMap;
import java.util.List;

public class ReturnService {


    public boolean returnItem(int issuedItemId, List<Item> items, HashMap<Item, Customer> checkedOutItems, Customers customers) throws InvalidLoggedInUserException {

        for (Item item : checkedOutItems.keySet()) {
            if (item.getItemId() == issuedItemId) {

                if (checkedOutItems.get(item) != customers.getLoggedInCustomer())
                    throw new InvalidLoggedInUserException();
                checkedOutItems.remove(item);

                return items.add(item);
            }
        }
        return false;
    }
}
