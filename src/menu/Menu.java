package menu;

import customer.Customers;

import java.util.ArrayList;
import java.util.List;

public class Menu {

    private Customers customers;

    public Menu(Customers customers) {

        this.customers = customers;
    }

    public List<String> allMenuItems() {
        List<String> menuItems = new ArrayList<>();
        menuItems.add("\n*****Menu****");
        menuItems.add("1.Show all books.");
        menuItems.add("2.Show all movies.");
        menuItems.add("3.Checkout book.");
        menuItems.add("4.Checkout movie.");
        menuItems.add("5.Return book.");
        menuItems.add("6.Display customer details.");
        if (customers.getLoggedInCustomer() != null) {
            menuItems.add("7.Logout.");
        }
        else
        menuItems.add("7.Login");
        menuItems.add("0.Quit");

        return menuItems;
    }


}
