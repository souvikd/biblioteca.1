package menu;

import customer.Customers;
import menucommands.*;

import java.util.HashMap;

public class MenuActions {


    HashMap<Integer, Class<?>> commands;
    private Customers customers;

    public MenuActions(Customers customers) {
        this.customers = customers;
    }


    public void loadCommands() {
        commands = new HashMap<Integer, Class<?>>() {

            @Override
            public Class<?> get(Object key) {
                return containsKey(key) ? super.get(key) : DisplayErrorCommand.class;
            }

        };

        commands.put(1, ShowBooksCommand.class);
        commands.put(2, ShowMoviesCommand.class);
        commands.put(3, CheckoutBookCommand.class);
        commands.put(4, CheckoutMovieCommand.class);
        commands.put(5, ReturnBookCommand.class);
        commands.put(6, ShowUserdetailsCommand.class);

        if (customers.getLoggedInCustomer() != null)
            commands.put(7, CustomerLogoutCommand.class);
        else
            commands.put(7, CustomerLoginCommand.class);

        commands.put(0, ExitApplicationCommand.class);

    }


    public Class<?> getCommandClass(int choice) {
        loadCommands();
        return commands.get(choice);
    }
}
