package menu;

import io.InputReader;
import io.OutputWriter;

import java.util.List;

public class MenuView {

    private final InputReader reader;
    private final OutputWriter writer;

    public MenuView(InputReader reader, OutputWriter writer) {

        this.reader = reader;
        this.writer = writer;
    }

    public void showMenu(List<String> menuItems) {
        for (String item : menuItems)
            this.writer.write(item);
    }

    public int getSelectedOption() {
        return this.reader.readMenuChoice();
    }

    public void showInvalidInputMessage() {
        writer.write("Select a valid option!");
    }
}
