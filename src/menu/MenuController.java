package menu;

import menucommands.MenuCommand;

import java.util.List;

public class MenuController {
    private final Menu menu;
    private MenuView menuView;

    public MenuController(Menu menu, MenuView menuView) {
        this.menu = menu;
        this.menuView = menuView;
    }

    public void showMenu() {
        List<String> menuItems = menu.allMenuItems();
        menuView.showMenu(menuItems);
    }

    public int getUserChoice() {

        return menuView.getSelectedOption();
    }

    public void showInvalidInputMessage() {
        menuView.showInvalidInputMessage();
    }
}
