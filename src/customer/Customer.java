package customer;

public class Customer {

    private final String name;
    private String userId;
    private final String phoneNumber;
    private final String email;
    private final String password;

    public Customer(String name, String userId, String phoneNumber, String email, String password) {

        this.name = name;
        this.userId = userId;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
    }

    public String details() {
        return name + " " + phoneNumber + " " + email;
    }

    public boolean verifyPassword(String password) {
        return this.password.equals(password);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        return userId.equals(customer.userId);

    }

    @Override
    public int hashCode() {
        return userId.hashCode();
    }
}
