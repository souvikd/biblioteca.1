package customer;

import java.util.HashMap;

public class Customers {

    HashMap<String, Customer> customers = new HashMap<>();
    Customer loggedInCustomer = null;

    public void loadAllCustomers() {
        customers.put("123-1234", new Customer("Souvik", "123-1234", "9049766840", "souvik014@gmail.com", "password"));
    }


    public boolean verifyCustomer(String customerId, String password) {

        if (customers.containsKey(customerId) && customers.get(customerId).verifyPassword(password)) {
            loggedInCustomer = customers.get(customerId);
            return true;
        } else
            return false;
    }

    public Customer getLoggedInCustomer() {
        return loggedInCustomer;
    }


    public void logoutCurrentlyLoggedInCustomer() {
        loggedInCustomer = null;
    }


    public String getCurrentUserDetails() {
        if (loggedInCustomer != null)
            return loggedInCustomer.details();
        else
            return null;
    }
}
