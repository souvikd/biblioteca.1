package library;

import io.InputReader;
import io.OutputWriter;

import java.util.ArrayList;
import java.util.List;

public class LibraryView {

    private InputReader reader;
    private final OutputWriter writer;

    public LibraryView(InputReader reader, OutputWriter writer) {
        this.reader = reader;

        this.writer = writer;
    }


    public void showItems(List<String> items) {
        for (String itemInfo : items)
            writer.write(itemInfo);
    }


    public String getNameOfItemToBeCheckedout() {
        writer.write("Enter name of book");
        String nameOfBookToBeCheckedout = reader.readString();
        return nameOfBookToBeCheckedout;
    }

    public void displayItemNotFoundMessage() {
        writer.write("It seems the book you are requesting for is unavailable.");
    }

    public void showItemCheckedoutMessage(String details) {
        writer.write("Checkedout : " + details);
    }

    public int getIdOfItemToBeReturned() {
        writer.write("Enter ID of Book.");
        return reader.getIdOfBookToBeReturned();
    }

    public void showItemSuccessfullyReturnedMessage() {
        writer.write("Book returned successfully.");
    }

    public void displayInvalidItemError() {
        writer.write("Invalid book.");
    }


    public void displayUserNotLoggedInMessage() {
        writer.write("Please log in to continue.");
    }
}
