package library;

import Items.Item;
import Items.ItemNotFoundException;
import Items.Items;
import authentication.InvalidLoggedInUserException;

import java.util.List;

public class LibraryController {

    private final LibraryView libraryView;
    private final Items items;

    public LibraryController(Items items, LibraryView libraryView) {
        this.libraryView = libraryView;
        this.items = items;
    }

    public void loadItemsIntoLibrary() {
        items.loadAllItemsIntoLibrary();
    }


    public void showItems() {
        List<String> detailsOfAllItems = items.detailsOfAllItems();
        libraryView.showItems(detailsOfAllItems);
    }

    public void quit() {
        System.exit(0);
    }


    public void checkoutItem() {

        String itemName = libraryView.getNameOfItemToBeCheckedout();
        try {

            Item book = items.issueItem(itemName);
            libraryView.showItemCheckedoutMessage(book.details());

        } catch (ItemNotFoundException e) {

            libraryView.displayItemNotFoundMessage();
        } catch (InvalidLoggedInUserException e) {

            libraryView.displayUserNotLoggedInMessage();
        }

    }

    public void performItemReturn() {
        try {
            int itemId = libraryView.getIdOfItemToBeReturned();
            boolean itemReturned = items.returnItem(itemId);
            if (itemReturned)
                libraryView.showItemSuccessfullyReturnedMessage();
            else
                libraryView.displayInvalidItemError();

        } catch (InvalidLoggedInUserException e) {
        }
    }


    public void showCurrentlyLoggedInCustomerDetails() {

    }
}
